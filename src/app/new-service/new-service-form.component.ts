import { Component,OnInit,ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {KeyValueData} from './../datamodel/KeyValueData';
import {VoipNowService} from './../services/voipnow-service';
import {ServiceOrder} from './../datamodel/ServiceOrder';
import {AccountForm} from './../datamodel/AccountForm';

@Component({
  selector: 'new-service-form',
  templateUrl: './new-service-form.component.html'
})

export class NewserviceFormComponent  implements OnInit{
  public realDescription: any;
  public serviceOrder : ServiceOrder = new ServiceOrder();

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  constructor(private route: ActivatedRoute,private  service : VoipNowService) {

  }

  selectedServiceOrderType : string;
  serviceOrderTypes: KeyValueData[] = [];

  selectedServicePriority : string;
  serviceOrderPriorities : KeyValueData[] = [];

  selectedServiceOrderDescription : string;
  serviceOrderDescriptions : string [] = [];

  selectedServiceOrderTerm : string;
  serviceOrderTerms : KeyValueData[] = [];

  isCCChanged : false;

  isCCChecked = false;
  isScheduleChecked = false;

  
  onCCChange(eve: any) {
    this.isCCChecked = !this.isCCChecked;
    console.log(this.isCCChecked);
  }

  onScheduleChange(eve: any) {
    this.isScheduleChecked = !this.isScheduleChecked;
    console.log(this.isScheduleChecked);
    console.log(this.serviceOrder.briefDescription);
  }

  onDescriptionChange(eve:any){

  }

  onTypeChange(eve:any){
  }

  onPriorityChange(eve:any){

  }

  onTermChange(eve:any){

  }

  ngOnInit(): void {    
    this.service.getMasterData("TYPE").then(c => this.serviceOrderTypes = c);
    this.service.getMasterData("PRIORITY").then(c => this.serviceOrderPriorities  = c);
    this.service.getMasterDataAsString("SERVICE_ORDER_DESCRIPTIONS").then(c => this.serviceOrderDescriptions  = c);
    this.service.getMasterData("TERM").then(c => this.serviceOrderTerms  = c);

    this.serviceOrder.serviceOrderType = 'Service Call';
    this.serviceOrder.priority= 'Medium';
    this.serviceOrder.invoiceTerms = 'COD';

    this.route.queryParams
    .filter(params => params.accountId)
    .subscribe(params => {
      console.log(params); 

    this.serviceOrder.accountNumber = params.accountId;
    });

  }

  search = (text$: Observable<string>) =>
      text$
        .debounceTime(200).distinctUntilChanged()
        .merge(this.focus$)
        .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
        .map(term => (term === '' ? this.serviceOrderDescriptions : this.serviceOrderDescriptions.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));
}

