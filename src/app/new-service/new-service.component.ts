import { Component,OnInit,ViewChild,AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import {ServiceOrder} from './../datamodel/ServiceOrder';
import {NewserviceFormComponent} from './new-service-form.component';
import {VoipNowService} from './../services/voipnow-service';
import {DatePipe } from '@angular/common';
import {UserService} from '../services/user-service';

@Component({
  selector: 'new-service',
  templateUrl: './new-service.component.html',
  styleUrls: ['./new-service.component.css'],
  providers: [DatePipe]
})

export class NewserviceComponent  {
  @ViewChild(NewserviceFormComponent) 
  private newServiceForm:NewserviceFormComponent;

  constructor(public datepipe: DatePipe,private route: ActivatedRoute,private service:VoipNowService,private userService: UserService){

  }

  isCCChanged : false;
  isCCChecked = false;
  isScheduleChecked = false;
  uname : string;

  accountId : number;

  onCCChange(eve: any) {
    this.isCCChecked = !this.isCCChecked;
    console.log(this.isCCChecked);
  }

  onScheduleChange(eve: any) {
    this.isScheduleChecked = !this.isScheduleChecked;
    console.log(this.isScheduleChecked);
  }

  ngOnInit() {
    this.userService.currentMessage.subscribe(uname => this.uname = uname);

    this.route.queryParams
      .filter(params => params.accountId)
      .subscribe(params => {
        console.log(params); 

        this.accountId = params.accountId;
        console.log('Account Number is: '+this.accountId);
      });
  }


  createSO(event){
    console.log('Starting to create new Service Order');
    console.log('the message is'+this.userService.getMessage());
    console.log(this.newServiceForm.serviceOrder);
    this.newServiceForm.serviceOrder.user = this.userService.getMessage();
    this.newServiceForm.serviceOrder.dateTimeReceived=this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.service.createServiceOrder(this.newServiceForm.serviceOrder);
  }
}

