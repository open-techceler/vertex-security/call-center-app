import {DateTimeRequested} from './DateTimeRequested';

export class ServiceOrder {
    accountNumber : number;
    briefDescription : string;
    invoiceTerms: string;
    workRequested : string;
    status : string;
    contactNumber : number;
    serviceOrderType: string;
    takenBy: string;
    dateTimeReceived : string;
    priority: string;
    dispatchBoardNumber: number;
    internalComment : string;
    timeRequested : string;
    dateRequested : string;
    user : string;
    ccnumber : string;
    ccexp: string;
    vcc : string;
    cctype : string;
    
}