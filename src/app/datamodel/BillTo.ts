export class BillTo {

 postalCode : string;
 city : string;
 state : string;
 country : string;
 address1 : string;
 location : string;
 streetNumber : string;
 streetName : string;
 isPrimary : boolean;
 name : string;
}