export class Account {
    firstName : string;
    lastName : string;
    phoneNumber : string;
    marketingSource : string;
    name : string;
    accountEmail : string;
    companyWebsite : string;
    accountProfile: string;
    contactPhone : string;
    contactTitle : string;
    contactEmail: string;
    postalCode : string;
    city : string;
    state: string;
    country : string;
    address1 : string;
    location : string;
    streetNumber: string;
    streetName:string;
    accountType:string;
    hotNote:string;

}