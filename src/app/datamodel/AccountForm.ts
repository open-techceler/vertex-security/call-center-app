import {ServiceOrder} from './../datamodel/ServiceOrder';
import {Account} from './../datamodel/Account';
import {JournalEntry} from './../datamodel/JournalEntry';
import {BillTo} from './../datamodel/BillTo';

export class AccountForm {
    serviceOrder : ServiceOrder;
    account : Account;
    journalEntry : JournalEntry;
    billTo : BillTo;
    user : string;
    message : string;
}