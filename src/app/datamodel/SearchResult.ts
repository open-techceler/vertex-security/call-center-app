export class SearchResult {
    accountId : number;
    accountName : string;
    contactName : string;
    phoneNumber : string;
    address : string;
    openJobs : number;
    openOpp : number;
    overdue : number;
}