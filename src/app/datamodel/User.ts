export class User {
    name : string;
    pwd : string;
	privateKey : string;
	publicKey : string;
    port : string;
    extension : string;
    vpuser : string;

public constructor(name:string,privateKey:string,publicKey:string,port:string,extension:string,vpuser:string){
		this.name = name;
		this.pwd = '';
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.extension = extension;
        this.port = port;
        this.vpuser = vpuser;
	}
}