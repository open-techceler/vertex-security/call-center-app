import { Component,Input  } from '@angular/core';

@Component({
  selector: 'nav-items',
  templateUrl: './nav.component.html',
})

export class NavComponent {
  @Input() accountId: string;
  @Input() name: string;
  @Input() phoneNumber: string;
  @Input() marketingSource: string;
  @Input() accountName: string;

  existingServiceOrderUrl : string;
  existingAccount : string;
  existingQuote : string;

    constructor(){
      var currentUser = JSON.parse(localStorage.getItem("currentUser"));  
      
      this.existingServiceOrderUrl  = "http://localhost:"+ currentUser.port + "/ServiceOrder?number={int%20SONumber}&showOrder={bool%20JustShowOrder}&saveFirst={bool%20DontSaveFirst";
      this.existingAccount = "http://localhost:"+ currentUser.port+"/Account/%7Bphone%20number";
      this.existingQuote = "http://localhost:"+currentUser.port +"/Opportunity/Opportunity Id";
    }
}
