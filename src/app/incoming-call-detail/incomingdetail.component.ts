import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {VoipNowService} from '../services/voipnow-service';
import 'rxjs/add/operator/filter';
import { SearchResult } from 'app/datamodel/SearchResult';

@Component({
  templateUrl: './incomingdetail.component.html',
  styleUrls: ['./incomingdetail.component.css']
})

export class IncomingdetailComponent implements OnInit{

constructor (private route: ActivatedRoute,private service :VoipNowService ){

}

searchResult : SearchResult [] = null; 
name : string;
selectedRow : Number;
selectedAccountId : Number;
marketingSource : string;
phoneNumber : string;
accountName : string;

selectRow(index:number){
  console.log(this.searchResult);
  console.log(this.searchResult[index].accountId);
  this.selectedAccountId = this.searchResult[index].accountId;
  this.marketingSource = "Vertex";
  this.phoneNumber = this.searchResult[index].phoneNumber;
  this.name = this.searchResult[index].contactName;
  this.accountName = this.searchResult[index].accountName;
  this.selectedRow = index;
}

ngOnInit() {
    this.route.queryParams
      .filter(params => params.name)
      .subscribe(params => {
        console.log(params); 

        this.name = params.name;
        console.log(this.name);
        this.service.search(this.name,null).then(c => this.searchResult = c);;
      });
  }


}
