import { Component,OnInit  } from '@angular/core';
import {VoipNowService} from '../services/voipnow-service';
import {IncomingCalls} from '../datamodel/IncomingCalls';
import {UserService} from '../services/user-service';

@Component({
  templateUrl: './incominglist.component.html',
  styleUrls: ['./incominglist.component.css']
})

export class IncominglistComponent {

  incomingCalls = new IncomingCalls();
  uname : string;

  constructor (private voipService : VoipNowService,private userService: UserService){
    this.loadData();
  }

  loadData(){
    this.voipService.getIncomingCalls().then(c => this.incomingCalls = c);
    console.log(localStorage.getItem("currentUser"));
  }



  ngOnInit() {
    this.userService.currentMessage.subscribe(uname => this.uname = uname);
  }


}
