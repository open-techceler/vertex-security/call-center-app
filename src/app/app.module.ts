import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { ButtonsModule } from 'ngx-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { IncominglistComponent } from './incoming-call-list/incominglist.component';
import { IncomingdetailComponent } from './incoming-call-detail/incomingdetail.component';
import { NewaccountComponent } from './new-account-form/newaccount.component';
import { NewserviceComponent } from './new-service/new-service.component';
import { NewserviceFormComponent } from './new-service/new-service-form.component';
import { CcOptionsComponent } from './new-service/cc-options.component';
import { ScheduleOptionsComponent } from './new-service/schedule-options.component';
import { CallTransferComponent } from './call-transfer/call-transfer.component';
import { NavComponent } from './common/nav.component';

import { AuthService } from './services/auth-service';
import { VoipNowService } from './services/voipnow-service';
import { UserService } from './services/user-service';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'incoming-list', component: IncominglistComponent },
  { path: 'search-results', component: IncomingdetailComponent },
  { path: 'new-account', component: NewaccountComponent },
  { path: 'new-service', component: NewserviceComponent },
  { path: 'call-transfer', component: CallTransferComponent },

  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IncominglistComponent,
    IncomingdetailComponent,
    NewaccountComponent,
    NewserviceComponent,
    CcOptionsComponent,
    ScheduleOptionsComponent,
    NewserviceFormComponent,
    CallTransferComponent,
    NavComponent
  ],
  imports: [
    GooglePlaceModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ButtonsModule.forRoot(),
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AuthService,VoipNowService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
