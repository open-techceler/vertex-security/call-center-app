import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {User} from '../datamodel/User';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

 public constructor(private http : Http,private router : Router) {}

 authenticate(username,password) : Promise<User> {
    console.log("authenticating...");
    console.log("calling service");

    var url = environment.backenduri+"/login";

    const body = {
      'username' : username, 'pwd':password
    }
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });


    console.log(environment.backenduri);
    return this.http.post(url, body, options)
    .toPromise()
    .then ((data) => 
      {
      let body = data.json();
      let authUser = new User(body.username,body.privateKey,body.publicKey,body.port,body.extension,body.vpuser);
      
      //Add the user into local storage
      localStorage.setItem('currentUser', JSON.stringify(authUser));

      //Navigate to the next page
      this.router.navigate(['/incoming-list']);
      return authUser;   
      }).catch(this.handleError);
    
    
}


private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
 return Promise.reject(error.message || error);
}

} 
