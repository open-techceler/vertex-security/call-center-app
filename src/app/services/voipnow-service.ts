import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import {User} from '../datamodel/User';
import {SearchResult} from '../datamodel/SearchResult';

import { Router } from '@angular/router';
import {IncomingCalls} from '../datamodel/IncomingCalls';
import { KeyValueData } from 'app/datamodel/KeyValueData';
import {ServiceOrder} from '../datamodel/ServiceOrder';
import {AccountForm} from './../datamodel/AccountForm';

@Injectable()
export class VoipNowService {

public constructor(private http : Http,private router : Router) {}

accessToken : string;

getIncomingCalls() : Promise<IncomingCalls>  {
  var currentUser = JSON.parse(localStorage.getItem("currentUser"));
  var url = environment.backenduri+"/listphonecalls?"+"userId="+currentUser.vpuser+"&extension="+currentUser.extension;
    
  let headers = new Headers({ 'Content-Type': 'application/json' });
  
  console.log("Sending request to :" + url);
  
  return this.http 
  .get(url)
  .toPromise()
  .then((data) => {//this.extractData;
  
  console.log('extracting data now');
  
  let body = data.json();
  let incomingCalls = new IncomingCalls();

  console.log(body);
  console.log("total results:"+body.totalResults);
  incomingCalls.totalCalls = body.totalResults;
  
  if (incomingCalls.totalCalls == "0"){
    console.log("0 incoming calls");
  }
  else {
    console.log("1 or more incoming calls");
    incomingCalls.totalCalls = "1";
  }
  return incomingCalls;  
  }).catch(this.handleError);
}

search(accountName:string,phoneNumber:string) : Promise<SearchResult[]>  {
  var currentUser = JSON.parse(localStorage.getItem("currentUser"));

  //var queryParam = "accountName="+name+"&phoneNumber="+phoneNumber;
  var queryParam = "name="+accountName;
  var url = environment.backenduri+"/findaccounts?"+queryParam;
    
  let headers = new Headers({ 'Content-Type': 'application/json' });
  
  console.log("Sending request to :" + url);
  
  let searchResultArray:SearchResult [] = [];
  return this.http 
  .get(url)
  .toPromise()
  .then((data) => {
  
  console.log('extracting data now');

  let body = data.json();
  for (let entry of body) {
    console.log(body); // 1, "string", false
    let searchResults = new SearchResult();
    searchResults.accountId = entry.accountId
    searchResults.accountName = entry.accountName;
    searchResults.contactName = entry.contactName;
    searchResults.phoneNumber = entry.phoneNumber;
    searchResults.address = entry.address;
    searchResults.openJobs = entry.openJobs;
    searchResults.openOpp = entry.openOpp;
    searchResults.overdue = entry.overdue; 
    console.log('entry.openJobs'+entry.openJobs)
    searchResultArray.push(searchResults);
  }

  return searchResultArray;  
  }).catch(this.handleError);
}

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
  }

  getMasterData(dataType : string) : Promise<KeyValueData[]>  {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    var url = environment.backenduri+"/data?"+"type="+dataType;
      
    let headers = new Headers({ 'Content-Type': 'application/json' });
    
    console.log("Sending request to :" + url);
    let  keyValueData : KeyValueData[] = [];

    return this.http 
    .get(url)
    .toPromise()
    .then((data) => {
    
    console.log('extracting data now');
    
    let body = data.json();
    for (let entry of body) {
      let kvdata = new KeyValueData();
      kvdata.name = entry.name;
      kvdata.value = entry.value;
      keyValueData.push(kvdata);
      console.log(body);   
    } 
    return keyValueData;  
    }).catch(this.handleError);
  }
  

  getMasterDataAsString(dataType : string) : Promise<string[]>  {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    var url = environment.backenduri+"/data?"+"type="+dataType;
      
    let headers = new Headers({ 'Content-Type': 'application/json' });
    
    console.log("Sending request to :" + url);
    let  values : String [] = [];

    return this.http 
    .get(url)
    .toPromise()
    .then((data) => {
    
    console.log('extracting data now');
    
    let body = data.json();
    for (let entry of body) {
      values.push(entry.name);
      console.log(body);   
    } 
    return values;  
    }).catch(this.handleError);
  }

  createServiceOrder(serviceOrder : ServiceOrder){
    var url = environment.backenduri+"/create-service";
    return this.http 
    .post(url,serviceOrder)
    .toPromise()
    .then((data) => {
    
    console.log('extracting data now');
    
    console.log(data);   
    //Navigate to the next page
    this.router.navigate(['/incoming-list']);
    
  }).catch(this.handleError);
  }

  createAccount(accountForm : AccountForm){
    var url = environment.backenduri+"/create-account";
    return this.http 
    .post(url,accountForm)
    .toPromise()
    .then((data) => {
    
    console.log('extracting data now');
    
   // let body = data.json();
    console.log(data);   
    accountForm.message = "Account Created Successfully.";    
    
    //Navigate to the next page
    this.router.navigate(['/incoming-list']);

  }).catch(this.handleError);
  }


  
} 
