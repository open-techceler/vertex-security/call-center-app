import { Component,OnInit,ViewChild,AfterViewInit} from '@angular/core';

import {KeyValueData} from './../datamodel/KeyValueData';
import {Account} from './../datamodel/Account';
import {JournalEntry} from './../datamodel/JournalEntry';
import {AccountForm} from './../datamodel/AccountForm';
import {BillTo} from './../datamodel/BillTo';

import {VoipNowService} from './../services/voipnow-service';
import {UserService} from './../services/user-service';

import {NewserviceFormComponent} from './../new-service/new-service-form.component';
import { ActivatedRoute } from '@angular/router';
import { GooglePlaceModule,GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import {DatePipe } from '@angular/common';

@Component({
  templateUrl: './newaccount.component.html',
  styleUrls: ['./newaccount.component.css'],
  providers: [DatePipe]
})

export class NewaccountComponent implements OnInit {
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;

  @ViewChild(NewserviceFormComponent) 
  private newServiceForm:NewserviceFormComponent;

  constructor(public datepipe: DatePipe,private service : VoipNowService,
    private route: ActivatedRoute,private userService: UserService){
  }

  isIndividualAccount = true;

  isBillingAddress = false;
  
  isNewService = false;

  isNewInstall = false;
  
  uname : string;

  isdisabled = false;

  public account : Account = new Account();
  public billTo : BillTo = new BillTo();
  public accountForm : AccountForm = new AccountForm();

  selectedAccountType : string;
  accountTypes = [ 
                  {value: 'Individual',name:'Individual'},
                  {value:'Company',name:'Company'}
                ];

  selectedSystemType : KeyValueData;
  systemTypes : KeyValueData[] = [];

  selectedCloseReason : KeyValueData;
  closeReasons: KeyValueData[] = [];

  selectedOpportunityName : KeyValueData;
  opportunityNames: KeyValueData[] = [];

  selectedTitles : KeyValueData;
  titles : KeyValueData[] = [];

  onAccountTypeChange (eve: any) {
    this.isIndividualAccount = !this.isIndividualAccount;
    //this.account.accountType = this.selectedAccountType;
    console.log("sdfsdfsdfsdfsdf"+this.account.accountType);
  }

  onBillingAddressChange (eve: any) {
    this.isBillingAddress = !this.isBillingAddress;
    console.log(this.isBillingAddress);

    this.billTo.postalCode="";
    this.billTo.location = "";
    this.billTo.city = "";
    this.billTo.streetNumber = "";
    this.billTo.streetName = "";
    this.billTo.state = "";
    this.billTo.country = "";

  }

  onNewServiceChange (eve: any) {
    this.isNewService = !this.isNewService;
    console.log(this.isNewService);
  }

onNewInstallChange (eve: any) {
    this.isNewInstall = !this.isNewInstall;
    console.log(this.isNewInstall);
  }

ngOnInit(): void {    
    this.userService.currentMessage.subscribe(uname => this.uname = uname);

    //this.service.getMasterData("ACCOUNT_TYPE").then(c => this.accountTypes = c);
    this.service.getMasterData("CLOSE_REASON").then(c => this.closeReasons = c);
    this.service.getMasterData("SYSTEM_TYPE").then(c => this.systemTypes = c);
    this.service.getMasterData("OPPORTUNITY_NAME").then(c => this.opportunityNames = c);
    this.service.getMasterData("TITLE").then(c => this.titles = c);

    this.account.accountType = this.accountTypes[0].value;
    console.log(this.accountTypes[0]);
    this.route.queryParams
      .subscribe(params => {
        console.log(params); 

        this.account.name = params.accountName;
        this.account.firstName = params.name;
        this.account.lastName = params.name;
        this.account.phoneNumber = params.phoneNumber;
        this.account.marketingSource = params.marketingSource;
        console.log('marketingsource'+this.account.marketingSource);

      }); 
}

createAccount(event){
    console.log('Starting to create new Account');
    this.accountForm.user = this.userService.getMessage();

    if (this.newServiceForm!=null){
      this.newServiceForm.serviceOrder.dateTimeReceived=this.datepipe.transform(new Date(), 'yyyy-MM-dd');
      this.accountForm.serviceOrder = this.newServiceForm.serviceOrder;
    }

    this.accountForm.account = this.account;
    console.log("sdf"+this.accountForm.account.accountType);

    if (this.billTo!=null){
      this.accountForm.billTo = this.billTo;
    }

    this.service.createAccount(this.accountForm);
  }

    
  public handleAddressChange(address: Address) {
    
    console.log(address);
    var comp = address.address_components;
    console.log(comp);
     this.account.address1 = address.formatted_address;

     this.account.postalCode="";
     this.account.location = "";
     this.account.city = "";
     this.account.streetNumber = "";
     this.account.streetName = "";
     this.account.state = "";
     this.account.country = "";

     for (var i =0 ; i<comp.length;i++){
       console.log(comp[i].long_name);
       console.log(comp[i].types[0]);

      if (comp[i].types[0]=='postal_code'){
        this.account.postalCode = comp[i].long_name; 
      }
      if (comp[i].types[0]=='locality'){
        this.account.location = comp[i].long_name; 
        this.account.city = comp[i].long_name; 
      }
      if (comp[i].types[0]=='street_number'){
        this.account.streetNumber = comp[i].long_name; 
      }
      if (comp[i].types[0]=='route'){
        this.account.streetName = comp[i].long_name; 
      }
      if (comp[i].types[0]=='administrative_area_level_1'){
        this.account.state = comp[i].short_name; 
      }
      if (comp[i].types[0]=='country'){
        this.account.country = comp[i].long_name; 
      }

     }    
  }

  public handleBillingAddressChange(billingAddress: Address) {
    
    console.log(billingAddress);
    var comp  = billingAddress.address_components;
    console.log(comp);
     this.billTo.address1 = billingAddress.formatted_address;

     this.billTo.postalCode="";
     this.billTo.location = "";
     this.billTo.city = "";
     this.billTo.streetNumber = "";
     this.billTo.streetName = "";
     this.billTo.state = "";
     this.billTo.country = "";
 
     for (var i =0 ; i<comp.length;i++){
       console.log(comp[i].long_name);
       console.log(comp[i].types[0]);

      if (comp[i].types[0]=='postal_code'){
        this.billTo.postalCode = comp[i].long_name; 
      }
      if (comp[i].types[0]=='locality'){
        this.billTo.location = comp[i].long_name; 
        this.billTo.city = comp[i].long_name; 
      }
      if (comp[i].types[0]=='street_number'){
        this.billTo.streetNumber = comp[i].long_name; 
      }
      if (comp[i].types[0]=='route'){
        this.billTo.streetName = comp[i].long_name; 
      }
      if (comp[i].types[0]=='administrative_area_level_1'){
        this.billTo.state = comp[i].short_name; 
      }
      if (comp[i].types[0]=='country'){
        this.billTo.country = comp[i].long_name; 
      }

     }    
  }
  
}