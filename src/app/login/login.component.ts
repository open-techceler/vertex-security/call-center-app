import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth-service';
import { User } from '../datamodel/User';
import { UserService } from "../services/user-service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  notfound = false;
  pwd : string;
  uname : string;
  user : User;
  constructor(
    private router: Router,private authService : AuthService,private userService: UserService
  ) {}
  
  login(event){
    console.log("Login successful....");
    console.log("Routing to incoming call list");
         // this.router.navigate(['/incoming-list'], { fragment: 'top' });
      this.userService.changeMessage(this.uname);
     this.authService.authenticate(this.uname,this.pwd).then(c  => this.user = c);
      if (this.user!=null){
        localStorage.setItem('currentUser', JSON.stringify(this.user));
      }
      else {
       // this.notfound = true;
      }
        console.log(this.pwd);
        console.log(this.uname);
  }

  ngOnInit(){
    this.userService.currentMessage.subscribe(uname => this.uname = uname);
  }

  logout(event) {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
}

} 
