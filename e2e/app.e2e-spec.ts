import { CallCenterAppPage } from './app.po';

describe('call-center-app App', () => {
  let page: CallCenterAppPage;

  beforeEach(() => {
    page = new CallCenterAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
